// Выведем в подготовленную разметку данные пользователей полученные
// от сервиса https://randomuser.me/ Обеспечьте сортировку по имени.
//     После загрузки список выводится в неотсортированном виде. Сортировка
// начинается только после нажатия кнопки (сортироваться должен именно
// тот список который был загружен при запуске нашего приложения).
// Для вашего удобства:
//     https://randomuser.me/api/?seed=speciallist&results=25 будет давать всегда
//         один и тот же набор пользователей
// <!-- One Card -->
// <!--			<div class="row border-bottom pb-2 mt-2">-->
// <!--				<div class="col-3">Иван</div>-->
//     <!--				<div class="col-3">+380675558823</div>-->
//     <!--				<div class="col-3">London, United Kingdom</div>-->
// <!--			</div>-->
// <!-- /One Card -->
//вынести в ф-ю
//передавайть в нее массив
//а сам масив сортировать в icon.addEventListener("click", function() {
let url='https://randomuser.me/api/?seed=speciallist&results=25' ;
(async function(){
    let result=await fetch(url);
    result=await result.json();
    result=result.results;

    let element=document.querySelector('.cards')

    function createRow(fullName,fullPhone,fullCountry) {
        let name = document.createElement('div');
        name.className = 'col-3';
        name.innerHTML = fullName;

        let phone = document.createElement('div');
        phone.className = 'col-3';
        phone.innerHTML = fullPhone;

        let country = document.createElement('div');
        country.className = 'col-3';
        country.innerHTML = fullCountry;

        let row = document.createElement('div');
        row.className = 'row border-bottom pb-2 mt-2';

        row.appendChild(name);
        row.appendChild(phone);
        row.appendChild(country);
        return row;
    }

    function compareNumericMax(a, b) {
        if (a.name.first > b.name.first) return 1;
        if (a.name.first === b.name.first) return 0;
        if (a.name.first <b.name.first) return -1;
    }
    // result.sort(compareNumericMax);

    function compareNumericMin(a,b){
        if (a.name.first < b.name.first) return 1;
        if (a.name.first === b.name.first) return 0;
        if (a.name.first >b.name.first) return -1;
    }
    // result.sort(compareNumericMin);

    function show(res,elem) {
        elem.innerHTML='';
        res.map(el => {
            let fullName = `${el.name.first} ${el.name.last}`;
            let fullPhone = `${el.phone}`;
            let fullCountry = `${el.location.city} ${el.location.country}`;
             elem.appendChild(createRow(fullName, fullPhone, fullCountry));
        });
    };

    show(result,element);

    let icon=document.querySelector('i');

    icon.addEventListener("click", function() {
        if (this.classList.contains("text-gray")) {
            this.classList.remove("text-gray");
            this.classList.add("text-black");
            show(result.sort(compareNumericMax),element);
        } else if(this.classList.contains("fa-sort-amount-up-alt")) {
            this.classList.remove("fa-sort-amount-up-alt");
            this.classList.add("fa-sort-amount-down-alt");
            show(result.sort(compareNumericMin),element);
        }
        else {
            this.classList.remove("fa-sort-amount-down-alt");
            this.classList.add("fa-sort-amount-up-alt");
            show(result.sort(compareNumericMax),element);
        }
    });

})();